//
//  PDFDocumentModelTests.swift
//  PDFWrapperTests
//
//  Created by Muri Gumbodete on 04/04/2024.
//

import XCTest
import PDFKit

@testable import ShonaHymnBook

class PDFDocumentModelTests: XCTestCase {
    var pdfDocumentModel: PDFDocumentModel!
    var pdfViewWrapper: PDFViewWrapper!
    var initialPage: Int!

    override func setUp() {
        super.setUp()

        // Given
        pdfDocumentModel = PDFDocumentModel()
        initialPage = 0
        pdfViewWrapper = PDFViewWrapper(pdfDocumentModel: pdfDocumentModel, initialPage: initialPage)
    }

    override func tearDown() {
        pdfDocumentModel = nil
        pdfViewWrapper = nil

        super.tearDown()
    }

    func testPDFDocumentModelInitialization() {
        // Then
        XCTAssertNotNil(pdfDocumentModel)
    }

    func testPDFViewWrapperInitialization() {
        // Then
        XCTAssertNotNil(pdfViewWrapper)
        XCTAssertEqual(pdfViewWrapper.initialPage, initialPage)
        XCTAssertEqual(pdfViewWrapper.pdfDocumentModel, pdfDocumentModel)
    }

    func testPDFDocumentModelPDFDocument() {
        // Given
        let expectation = XCTestExpectation(description: "PDF document loaded")

        // When
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Then
            XCTAssertNotNil(self.pdfDocumentModel.pdfDocument)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5)
    }
}
