//
//  HymnListViewModelTests.swift
//  ShonaHymnBookTests
//
//  Created by Muri Gumbodete on 20/04/2024.
//

import XCTest
import Combine
@testable import ShonaHymnBook

class HymnListViewModelTests: XCTestCase {

    var sut: HymnListViewModel!
    var cancellables: Set<AnyCancellable>!

    override func setUp() {
        super.setUp()
        sut = HymnListViewModel()
        cancellables = Set<AnyCancellable>()
    }

    override func tearDown() {
        sut = nil
        cancellables = nil
        super.tearDown()
    }

    // Test initialization
    func testViewModelInitialization() {
        XCTAssertNotNil(sut)
        XCTAssertNotNil(sut.hymnBookRepository)
        XCTAssertTrue(sut.hymnViewModels.isEmpty)
    }

    func testHymnViewModelsUpdatedWhenHymnsChange() {
        // Given
        let hymn1 = Hymn(
            id: "1",
            hymnNumber: 1,
            altHymnNumber: 1,
            category: "Category",
            key: "Key",
            title: "Title",
            tune: "Tune",
            verses: ["Verse"],
            chorus: "Chorus"
        )

        let hymn2 = Hymn(
            id: "2",
            hymnNumber: 2,
            altHymnNumber: 2,
            category: "Category",
            key: "Key",
            title: "Title",
            tune: "Tune",
            verses: ["Verse"],
            chorus: "Chorus"
        )

        let expectation = XCTestExpectation(description: "HymnViewModels updated")

        var receivedHymnViewModels: [HymnViewModel] = []

        sut.$hymnViewModels
            .sink { hymnViewModels in
                receivedHymnViewModels = hymnViewModels
                expectation.fulfill()
            }
            .store(in: &cancellables)

        // When
        sut.hymnBookRepository.hymns = [hymn1, hymn2]

        // Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertEqual(receivedHymnViewModels.count, 2)
        XCTAssertEqual(receivedHymnViewModels[0].hymn, hymn1)
        XCTAssertEqual(receivedHymnViewModels[1].hymn, hymn2)
    }
}
