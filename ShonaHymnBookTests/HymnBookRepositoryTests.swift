//
//  HymnBookRepositoryTests.swift
//  ShonaHymnBookTests
//
//  Created by Muri Gumbodete on 20/04/2024.
//

import XCTest

@testable import ShonaHymnBook

class HymnBookRepositoryTests: XCTestCase {

    var sut: HymnBookRepository!

    override func setUp() {
        super.setUp()
        sut = HymnBookRepository()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testGetHymns() {
        // Create an expectation for a background task
        let expectation = XCTestExpectation(description: "Retrieve hymns")

        // Call getHymns() and assert the result
        sut.getHymns()

        // Wait for the expectation to be fulfilled
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            XCTAssertFalse(self.sut.hymns.isEmpty)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5)
    }
}
