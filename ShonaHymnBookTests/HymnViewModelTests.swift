//
//  HymnViewModelTests.swift
//  ShonaHymnBookTests
//
//  Created by Muri Gumbodete on 27/04/2024.
//

import XCTest
import Combine

@testable import ShonaHymnBook

final class HymnViewModelTests: XCTestCase {
    var sut: HymnViewModel!
    var cancellables: Set<AnyCancellable> = []

    override func setUp() {
        super.setUp()
        sut = HymnViewModel(hymn: testData[0])
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testFormattedVerses() {
        let formattedVerses = sut.formattedVerses
        XCTAssertEqual(formattedVerses.count, testData[0].verses.count)

        for (index, verse) in formattedVerses.enumerated() {
            let lines = testData[0].verses[index].components(separatedBy: "\n").map {
                $0.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            let firstLine = lines.first ?? ""
            let restOfLines = Array(lines.dropFirst())

            XCTAssertEqual(verse.firstLine, firstLine)
            XCTAssertEqual(verse.restOfLines, restOfLines)
        }
    }

    func testFormattedChorus() {
        let formattedChorus = sut.formattedChorus
        XCTAssertEqual(formattedChorus, testData[0].chorus?.replacingOccurrences(of: "\\n", with: "\n"))
    }
}
