# Shona Hymn Book (Ngoma)

## Overview

This project provides a SwiftUI-based PDF viewer for the "Shona Hymn Book" PDF document. Additionally, it intends to integrate with Firestore to retrieve hymn lyrics and display them alongside the PDF viewer. Users can browse through the pages of the hymn book, view each page, and access the corresponding hymn lyrics.

## Features

- Display the pages of the "Shona Hymn Book" PDF document.
- Navigate through pages using swipe gestures.
- Select a page to view in detail.
- Retrieve hymn lyrics from Firestore.
- Display hymn lyrics alongside the PDF viewer.

## Installation

To use this project, follow these steps:

1. Clone the repository to your local machine:

git clone git@bitbucket.org:MuriGee/shonahymnbook.git

2. Open the project in Xcode.

3. Build and run the project.

## Usage

Upon launching the app, you'll be presented with a collection of thumbnails representing each page of the "Shona Hymn Book" PDF. You can scroll through the thumbnails and tap on any page to view it in detail. Swipe left or right to navigate between pages. Additionally, you can access the lyrics of the hymn corresponding to the displayed page.

## Requirements

- iOS 14.0+
- Xcode 12.0+
- Swift 5.3+

## Dependencies

This project uses the following dependencies:

- SwiftUI: A modern way to declare user interfaces for any Apple platform.
- PDFKit: A framework for rendering and interacting with PDFs on iOS and macOS.
- Firestore: A flexible, scalable database for mobile, web, and server development.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgements

- The "Shona Hymn Book" PDF document is sourced from United Methodist Church Ngoma hymn book.

## Roadmap

While the current version of the project offers basic PDF viewing functionality and integration with Firestore for hymn lyrics, future updates may include:

- Enhanced UI for displaying hymn lyrics.
- Offline caching of hymn lyrics for improved performance.
- Search functionality.
- Bookmarking favorite hymns.

If you have any suggestions or feature requests, feel free to open an issue or submit a pull request.

