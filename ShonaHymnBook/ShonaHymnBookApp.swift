//
//  ShonaHymnBookApp.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 29/01/2024.
//

import SwiftUI
import Firebase

@main
struct ShonaHymnBookApp: App {
    // Initialize Firebase in the init method
    init() {
        FirebaseApp.configure()
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
