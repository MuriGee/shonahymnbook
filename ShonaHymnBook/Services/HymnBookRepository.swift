//
//  HymnBookRepository.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 16/04/2024.
//

import Combine
import FirebaseFirestore
import FirebaseFirestoreSwift

/// A repository for managing hymn data.
class HymnBookRepository: ObservableObject {
    private let path = "OldHymnBook"
    private let store = Firestore.firestore()
    @Published var hymns: [Hymn] = []

    /// Initializes the HymnBookRepository and fetches the hymn data.
    init() {
        getHymns()
    }

    /// Fetches the hymn data from Firestore.
    func getHymns() {
        store.collection(path)
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("Error retrieving hymns \(error.localizedDescription)")
                    return
                }

                self.hymns = querySnapshot?.documents.compactMap { document in
                    try? document.data(as: Hymn.self)
                } ?? []
            }
    }
}
