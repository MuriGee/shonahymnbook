//
//  MainView.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 12/04/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            PagesCollectionView(viewModel: PagesViewModel(pdfDocument: PDFDocumentModel()))
                .tabItem {
                    Label("Pages", systemImage: "book")
                }
            HymnList()
                .tabItem {
                    Label("Hymns", systemImage: "doc.text.magnifyingglass")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
