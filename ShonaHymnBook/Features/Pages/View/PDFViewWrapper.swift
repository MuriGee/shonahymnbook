//
//  PDFKitView.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 18/03/2024.
//

import SwiftUI
import PDFKit

/// A SwiftUI view that displays a PDF document from a given `PDFDocumentModel`.
struct PDFViewWrapper: UIViewRepresentable {
    @ObservedObject var pdfDocumentModel: PDFDocumentModel
    @State private var currentPageIndex: Int
    let initialPage: Int

    /// Initializes the `PDFViewWrapper`.
    /// - Parameters:
    ///   - pdfDocumentModel: The `PDFDocumentModel` containing the PDF document to display.
    ///   - initialPage: The page number to open the PDF at.
    init(pdfDocumentModel: PDFDocumentModel, initialPage: Int) {
        self.pdfDocumentModel = pdfDocumentModel
        self._currentPageIndex = State(initialValue: initialPage)
        self.initialPage = initialPage
    }

    /// Creates the `PDFView` and configures swipe gesture recognizers.
    func makeUIView(context: Context) -> PDFView {
        let pdfView = PDFView()
        if let pdfDocument = pdfDocumentModel.pdfDocument {
            loadPDF(pdfDocument, into: pdfView, initialPage: initialPage)
        }

        // Add swipe gesture recognizers
        let swipeLeftGesture = UISwipeGestureRecognizer(
            target: context.coordinator,
            action: #selector(context.coordinator.swipeLeft(_:))
        )
        swipeLeftGesture.direction = .left
        pdfView.addGestureRecognizer(swipeLeftGesture)

        let swipeRightGesture = UISwipeGestureRecognizer(
            target: context.coordinator,
            action: #selector(context.coordinator.swipeRight(_:))
        )
        swipeRightGesture.direction = .right
        pdfView.addGestureRecognizer(swipeRightGesture)

        return pdfView
    }

    /// Updates the `PDFView`.
    func updateUIView(_ uiView: PDFView, context: Context) {
        // No updates needed
    }

    /// Loads a PDF document into the specified `PDFView` and navigates to the initial page.
    private func loadPDF(_ pdfDocument: PDFDocument, into pdfView: PDFView, initialPage: Int) {
        pdfView.document = pdfDocument
        pdfView.displayMode = .singlePage
        pdfView.autoScales = true
        if let page = pdfDocument.page(at: initialPage) {
            pdfView.go(to: page)
        } else {
            print("Invalid page number: \(initialPage)")
        }
    }

    /// Creates and returns a coordinator.
    func makeCoordinator() -> Coordinator {
        return Coordinator(parent: self)
    }

    /// A coordinator that handles swipe gestures and page navigation.
    class Coordinator: NSObject {
        var currentPageIndex: Int = 0
        var parent: PDFViewWrapper

        init(parent: PDFViewWrapper) {
            self.parent = parent
        }

        /// Handles the left swipe gesture.
        @objc func swipeLeft(_ gesture: UISwipeGestureRecognizer) {
            // Handle left swipe
            print("Left swipe detected!")
            if parent.currentPageIndex < (parent.pdfDocumentModel.pdfDocument?.pageCount ?? 0) - 1 {
                parent.currentPageIndex += 1
                if let pdfView = gesture.view as? PDFView, let page = parent.pdfDocumentModel.pdfDocument?.page(
                    at: parent.currentPageIndex
                ) {
                    pdfView.go(to: page)
                }
            }
        }

        /// Handles the right swipe gesture.
        @objc func swipeRight(_ gesture: UISwipeGestureRecognizer) {
            // Handle right swipe
            print("Right swipe detected!")
            if parent.currentPageIndex > 0 {
                parent.currentPageIndex -= 1
                if let pdfView = gesture.view as? PDFView, let page = parent.pdfDocumentModel.pdfDocument?.page(
                    at: parent.currentPageIndex
                ) {
                    pdfView.go(to: page)
                }
            }
        }
    }
}
