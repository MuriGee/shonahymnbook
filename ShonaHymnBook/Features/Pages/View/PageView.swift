//
//  CardView.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 29/01/2024.
//

import SwiftUI

struct PageView: View {
    let image: Image
    let pageNumber: Int?

    var body: some View {
        VStack {
            image
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 100, height: 150)
                .cornerRadius(8)
                .shadow(radius: 3)
            if let pageNumber = pageNumber {
                Text("\(pageNumber)")
                    .foregroundColor(.black)
            }
        }
        .overlay(
            RoundedRectangle(cornerRadius: 8)
                .stroke(Color.gray, lineWidth: 1)
        )
    }
}

struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        let image = Image(uiImage: UIImage(named: "Page 1")!)
        PageView(image: image, pageNumber: 1)
    }
}
