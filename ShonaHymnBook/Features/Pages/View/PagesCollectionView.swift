//
//  ContentView.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 29/01/2024.
//

import SwiftUI

struct PagesCollectionView: View {
    @ObservedObject var viewModel: PagesViewModel

    let columns = [
        GridItem(.adaptive(minimum: 115))
    ]

    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: columns) {
                    ForEach(0..<(viewModel.pdfDocument.pdfDocument?.pageCount ?? 0) + 1, id: \.self) { index in
                        if let thumbnailImage = viewModel.pdfDocument.pdfDocument?.page(at: index) {
                            NavigationLink(
                                destination: PDFViewWrapper(pdfDocumentModel: viewModel.pdfDocument, initialPage: index)
                            ) {
                                PageView(
                                    image: Image(
                                        uiImage: thumbnailImage.thumbnail(
                                            of: CGSize(width: 100, height: 150),
                                            for: .cropBox)
                                    ),
                                    pageNumber: index + 1
                                )
                            }
                        }
                    }
                }
            }
            .navigationTitle("Ngoma")
        }
    }
}

struct PagesCollectionView_Previews: PreviewProvider {
    static var previews: some View {
        let pdfDocument = PDFDocumentModel()
        let viewModel = PagesViewModel(pdfDocument: pdfDocument)
        PagesCollectionView(viewModel: viewModel)
    }
}
