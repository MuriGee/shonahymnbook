//
//  PagesViewModel.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 22/03/2024.
//

import PDFKit
import SwiftUI

class PagesViewModel: ObservableObject {
    @Published var pdfDocument: PDFDocumentModel

    init(pdfDocument: PDFDocumentModel) {
        self.pdfDocument = pdfDocument
    }
}
