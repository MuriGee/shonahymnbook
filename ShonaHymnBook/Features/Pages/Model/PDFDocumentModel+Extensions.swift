//
//  PDFDocumentModel+Extensions.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 04/04/2024.
//

extension PDFDocumentModel: Equatable {
    static func == (lhs: PDFDocumentModel, rhs: PDFDocumentModel) -> Bool {
        return lhs.pdfDocument == rhs.pdfDocument
        && lhs.pdfDocument == rhs.pdfDocument
    }
}
