//
//  PDFDocumentModel.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 22/03/2024.
//

import PDFKit
import SwiftUI

class PDFDocumentModel: ObservableObject {
    @Published var pdfDocument: PDFDocument?

    init() {
        if let pdfURL = Bundle.main.url(forResource: "Shona Hymn Book", withExtension: "pdf") {
            pdfDocument = PDFDocument(url: pdfURL)
        }
    }
}
