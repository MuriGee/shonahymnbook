//
//  HymnList.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 12/04/2024.
//

import SwiftUI

struct HymnList: View {
    @ObservedObject var viewModel = HymnListViewModel()

    var body: some View {
        NavigationView {
            List(viewModel.hymnViewModels) { hymnViewModel in
                NavigationLink(destination: HymnDetailView(hymnViewModel: hymnViewModel)) {
                    HymnRow(viewModel: hymnViewModel)
                }
            }
            .navigationTitle("Hymns")
        }
    }
}

struct HymnList_Previews: PreviewProvider {
    static var previews: some View {
        HymnList()
    }
}
