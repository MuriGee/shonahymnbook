//
//  HymnRow.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 12/04/2024.
//

import SwiftUI

struct HymnRow: View {
    @ObservedObject var viewModel: HymnViewModel

    var body: some View {
        HStack {
            Text("\(viewModel.hymn.hymnNumber)").bold()
            Text("(\(viewModel.hymn.altHymnNumber))")
            Text(viewModel.hymn.title).bold()
        }
    }
}

struct HymnRow_Previews: PreviewProvider {
    static var previews: some View {
        let hymn = testData[0]
        let hymnViewModel = HymnViewModel(hymn: hymn)
        return HymnRow(viewModel: hymnViewModel)
    }
}
