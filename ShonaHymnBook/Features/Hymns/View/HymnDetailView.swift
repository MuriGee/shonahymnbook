//
//  HymnDetailView.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 20/04/2024.
//

import SwiftUI

struct HymnDetailView: View {
    let hymnViewModel: HymnViewModel

    var body: some View {
            ScrollView {
                VStack(spacing: 20) {
                    HStack {
                        Text("Doh is \(hymnViewModel.hymn.key)")
                        Spacer()
                        Text("Tune: \(hymnViewModel.hymn.tune)")
                    }

                    ForEach(hymnViewModel.formattedVerses.indices, id: \.self) { index in
                        VStack(alignment: .leading) {
                            HStack {
                                Text("\(index + 1):")
                                    .font(.headline)
                                Text(hymnViewModel.formattedVerses[index].firstLine)
                                    .font(.title3)
                            }
                            ForEach(
                                hymnViewModel.formattedVerses[index].restOfLines, id: \.self
                            ) { line in
                                Text(line)
                                    .font(.title3)
                            }
                        }

                        if let chorus = hymnViewModel.formattedChorus {
                            HStack {
                                Text("Korusi:")
                                Text(chorus)
                                    .italic()
                            }
                        }
                    }
                }
                .padding(.horizontal)
                .navigationTitle(hymnViewModel.hymn.title)
            }
    }
}

struct HymnDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let hymn = testData[0]
        let hymnViewModel = HymnViewModel(hymn: hymn)
        return HymnDetailView(hymnViewModel: hymnViewModel)
    }
}
