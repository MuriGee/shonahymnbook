//
//  HymnModel.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 12/04/2024.
//

import FirebaseFirestoreSwift

/// A struct representing a hymn.
struct Hymn: Codable, Identifiable {
    @DocumentID var id: String?
    var hymnNumber: Int
    var altHymnNumber: Int
    var category: String
    var key: String
    var title: String
    var tune: String
    var verses: [String]
    var chorus: String?
}

// Test data for debugging purposes
#if DEBUG
let testData = (1...10).map { hymnItem in
    Hymn(id: "id: \(hymnItem)",
         hymnNumber: hymnItem,
         altHymnNumber: hymnItem,
         category: "Rukoko",
         key: "E",
         title: "M’tsvene, M’tsvene, M’tsvene",
         tune: "Nicaea",
         verses: [
                     "M’tsvene, M’tsvene, M’tsvene \\nMwari Samasimba! \\nTinomukumbira pa magwanani ese; \\nM’tsvene, m’tsvene, m’tsvene, Mune nyasha huru, \\nUmwe vatatu pamwe chete zve!",
                     "M’tsvene, M’tsvene, M’tsvene \\nVese vanodaro, \\nVakahwararira pamberi penyu Mwari; \\nM’tsvene, m’tsvene, m’tsvene! \\nVanomunamata, \\nMwakange muri munozova zwe!",
                     "M’tsvene, M’tsvene, M’tsvene, \\nTichazomuona \\nMbiri yenyu huru panyika nekudenga; \\nM’tsvene, m’tsvene, m’tsvene! \\nMuri m’tsvene chete, \\nMwakanakisa, mwakachena zve!",
                     "M’tsvene, M’tsvene, M’tsvene! \\nZvese zvakasikwa \\nZvinokubza imwi mukuru kune vese, \\nM’tsvene, m’tsvene, m’tsvene! \\nMwakaruramisa, \\nMwari mupenyu nemusiki zve!"
                  ],
         chorus: "Tenzi, uyai zvino pano, \\nTakamumirira isu."
    )
}
#endif
