//
//  Hymn+Extensions.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 20/04/2024.
//

/// Extends the Hymn struct to conform to the Equatable protocol for equality comparison.
extension Hymn: Equatable {
    /// Checks if two Hymn objects are equal by comparing their properties.
    /// - Parameters:
    ///   - lhs: The left-hand side Hymn object.
    ///   - rhs: The right-hand side Hymn object.
    /// - Returns: True if the two Hymn objects are equal, otherwise false.
    static func == (lhs: Hymn, rhs: Hymn) -> Bool {
        return lhs.id == rhs.id &&
            lhs.hymnNumber == rhs.hymnNumber &&
            lhs.altHymnNumber == rhs.altHymnNumber &&
            lhs.category == rhs.category &&
            lhs.key == rhs.key &&
            lhs.title == rhs.title &&
            lhs.tune == rhs.tune &&
            lhs.verses == rhs.verses &&
            lhs.chorus == rhs.chorus
    }
}
