//
//  HymnListViewModel.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 16/04/2024.
//

import Combine

/// A view model for managing a list of hymns.
class HymnListViewModel: ObservableObject {
    @Published var hymnBookRepository = HymnBookRepository()
    @Published var hymnViewModels: [HymnViewModel] = []

    private var cancellables: Set<AnyCancellable> = []

    /// Initializes the HymnListViewModel and sets up bindings to update the hymnViewModels array when hymn data changes
    init() {
        hymnBookRepository.$hymns.map { hymns in
            hymns.map(HymnViewModel.init)
        }
        .assign(to: \.hymnViewModels, on: self)
        .store(in: &cancellables)
    }
}
