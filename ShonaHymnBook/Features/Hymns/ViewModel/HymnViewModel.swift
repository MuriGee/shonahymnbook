//
//  HymnViewModel.swift
//  ShonaHymnBook
//
//  Created by Muri Gumbodete on 16/04/2024.
//

import Combine

/// A view model representing a single hymn.
class HymnViewModel: ObservableObject, Identifiable {
    @Published var hymn: Hymn
    private var cancellables: Set<AnyCancellable> = []

    /// The unique identifier for the hymn.
    var id: String { hymn.id ?? "" }

    /// Formats the verses of the hymn into tuples containing the first line and the rest of the lines.
    var formattedVerses: [(firstLine: String, restOfLines: [String])] {
        return hymn.verses.map { verse in
            let lines = verse.replacingOccurrences(of: "\\n", with: "\n").components(separatedBy: "\n")
            let firstLine = lines.first ?? ""
            let restOfLines = Array(lines.dropFirst())
            return (firstLine, restOfLines)
        }
    }

    /// Formats the chorus of the hymn by replacing "\\n" with new lines.
    var formattedChorus: String? {
        guard let chorus = hymn.chorus else { return nil }
        return chorus.replacingOccurrences(of: "\\n", with: "\n")
    }

    /// Initializes the HymnViewModel with the provided hymn.
    /// - Parameter hymn: The hymn object to be represented by the view model.
    init(hymn: Hymn) {
        self.hymn = hymn
    }
}
